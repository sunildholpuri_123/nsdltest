package com.test.nsdl.mvvm.room

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.test.nsdl.mvvm.model.FavModel
import com.test.nsdl.mvvm.model.StoreModel

@Dao
interface DAOAccess {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun InsertData(storeModel: StoreModel)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun InsertFavData(storeModel: FavModel)

    @Query("DELETE FROM Fav WHERE paintId = :paintId AND shirtId=:shirtId")
    suspend fun DeleteFav(paintId: String,shirtId:String)


    @Query("SELECT * FROM Store WHERE Type =:type ORDER BY RANDOM()")
    fun getData(type: String?): LiveData<List<StoreModel>>

    @Query("SELECT * FROM Fav WHERE paintId =:paintID and shirtId=:shirtId")
    fun getFavDat(paintID: String?, shirtId: String): LiveData<FavModel>

}