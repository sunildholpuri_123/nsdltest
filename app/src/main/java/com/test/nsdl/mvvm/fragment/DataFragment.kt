package com.test.nsdl.mvvm.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide
import com.test.nsdl.mvvm.databinding.ItemLayoutBinding
import com.test.nsdl.mvvm.model.StoreModel


class DataFragment : Fragment() {
    lateinit var item: StoreModel

    companion object {
        private val PARAM = "PARAM"

        fun newInstance(param: StoreModel): DataFragment {
            val args: Bundle = Bundle()
            args.putSerializable(PARAM, param)

            val fragment = DataFragment()
            fragment.arguments = args
            return fragment
        }
    }
    var binding  : ItemLayoutBinding?=null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = ItemLayoutBinding.inflate(inflater, container, false)

        item = arguments?.getSerializable(PARAM) as StoreModel
        Glide.with(activity!!).load(item.Value)
            .into(binding!!.ivImage);
        return binding!!.root
    }

}
