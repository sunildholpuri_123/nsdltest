package com.test.nsdl.mvvm.view

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.viewpager.widget.ViewPager
import com.bumptech.glide.Glide
import com.github.dhaval2404.imagepicker.ImagePicker
import com.test.nsdl.mvvm.R
import com.test.nsdl.mvvm.adapter.ViewPagerAdapter
import com.test.nsdl.mvvm.constant.Constant
import com.test.nsdl.mvvm.databinding.ActivityMainBinding
import com.test.nsdl.mvvm.fragment.DataFragment
import com.test.nsdl.mvvm.model.StoreModel
import com.test.nsdl.mvvm.viewmodel.StoreViewModel
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*
import kotlin.collections.ArrayList

class MainActivity : AppCompatActivity(), View.OnClickListener {

    lateinit var storeViewModel: StoreViewModel

    lateinit var context: Context

    lateinit var shirtAdapet: ViewPagerAdapter
    lateinit var pantAdapet: ViewPagerAdapter

    var type = ""
    var isFav = false
    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        context = this@MainActivity
        storeViewModel = ViewModelProvider(this).get(StoreViewModel::class.java)
        init()
        getData(Constant.typePaint)
        getData(Constant.typeShirt)


    }

    fun init() {
        shirtAdapet =
            ViewPagerAdapter(supportFragmentManager)
        pantAdapet =
            ViewPagerAdapter(supportFragmentManager)
        binding.vpShirt.adapter = shirtAdapet
        binding.vpPant.adapter = pantAdapet
        binding.vpPant.setCurrentItem(0)
        binding.vpPant.offscreenPageLimit = 0
        binding.vpShirt.setCurrentItem(0)
        binding.vpShirt.offscreenPageLimit = 0
        binding.btnAddShirt.setOnClickListener(this)
        binding.btnPant.setOnClickListener(this)
        binding.btnFav.setOnClickListener(this)
        binding.ivRefresh.setOnClickListener(this)

        binding.vpShirt.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {}
            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {
                getFavDat()
            }

            override fun onPageSelected(position: Int) {
            }
        })

        binding.vpPant.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {}
            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {
                getFavDat()
            }

            override fun onPageSelected(position: Int) {
            }
        })
    }

    fun shirtData(lis: List<StoreModel>) {
        shirtAdapet.clearData()
        shirtAdapet.setList(lis)
        for (value in lis) {
            shirtAdapet.addFragment(DataFragment.newInstance(value))
        }
        shirtAdapet.notifyDataSetChanged()

    }


    fun pantData(lis: List<StoreModel>) {
        pantAdapet.clearData()
        pantAdapet.setList(lis)
        for (value in lis) {
            pantAdapet.addFragment(DataFragment.newInstance(value))
        }

        pantAdapet.notifyDataSetChanged()

    }


    fun getData(type: String) {
        val seed = System.currentTimeMillis()

        storeViewModel.getData(context, type)!!.observe(this, Observer {

            if (it != null) {

                if (type.equals(Constant.typePaint)) {
                    pantData(it)
                } else {
                    shirtData(it)
                }
            } else {
                Toast.makeText(this, getString(R.string.no_data_found), Toast.LENGTH_SHORT).show()
            }
        })
    }

    fun getFavDat() {
        if (pantAdapet.getList().size > 0 && shirtAdapet.getList().size > 0) {
            storeViewModel.getFavData(
                context,
                pantAdapet.getList().get(vpPant.currentItem).Id.toString(),
                shirtAdapet.getList().get(vpShirt.currentItem).Id.toString()
            )!!.observe(this, Observer {

                if (it != null) {
                    Toast.makeText(this, getString(R.string.match_found), Toast.LENGTH_SHORT).show()
                    Glide.with(this).load(R.drawable.ic_heartfill)
                        .into(btnFav);
                    isFav = true
                } else {
                    Glide.with(this).load(R.drawable.ic_heartunfill)
                        .into(btnFav);

                    Toast.makeText(this, getString(R.string.match_not_found), Toast.LENGTH_SHORT)
                        .show()
                    isFav = false
                }
            })
        }
    }

    override fun onClick(p0: View?) {
        when (p0?.id) {
            R.id.btnFav -> {

                if (isFav) {
                    storeViewModel.delFavItem(
                        context,
                        pantAdapet.getList().get(vpPant.currentItem).Id.toString(),
                        shirtAdapet.getList().get(vpShirt.currentItem).Id.toString()
                    )
                    Toast.makeText(this, getString(R.string.remove_fav), Toast.LENGTH_SHORT).show()
                } else {

                    storeViewModel.insertFavData(
                        context,
                        pantAdapet.getList().get(vpPant.currentItem).Id.toString(),
                        shirtAdapet.getList().get(vpShirt.currentItem).Id.toString()
                    )
                    Toast.makeText(this, getString(R.string.add_to_fav), Toast.LENGTH_SHORT).show()

                }


            }
            R.id.btnAddShirt -> {
                type = Constant.typeShirt
                getImage()


            }
            R.id.btnPant -> {
                type = Constant.typePaint
                getImage()

            }

            R.id.ivRefresh -> {
                /*getData(Constant.typePaint)
                getData(Constant.typeShirt)*/
            }
        }
    }

    fun getImage() {
        ImagePicker.with(this)
            .crop()                    //Crop image(Optional), Check Customization for more option
            .compress(1024)            //Final image size will be less than 1 MB(Optional)
            .maxResultSize(
                1080,
                1080
            )    //Final image resolution will be less than 1080 x 1080(Optional)
            .start()

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            val filePath: String = ImagePicker.getFilePath(data)!!
            if (type.equals(Constant.typePaint)) {
                storeViewModel.insertData(context, filePath, Constant.typePaint)
            } else {
                storeViewModel.insertData(context, filePath, Constant.typeShirt)

            }

        } else if (resultCode == ImagePicker.RESULT_ERROR) {
            Toast.makeText(this, ImagePicker.getError(data), Toast.LENGTH_SHORT).show()
        } else {
            Toast.makeText(this, getString(R.string.task_cancelled), Toast.LENGTH_SHORT).show()
        }
    }
}