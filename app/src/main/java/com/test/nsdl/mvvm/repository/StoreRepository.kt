package com.test.nsdl.mvvm.repository

import android.content.Context
import androidx.lifecycle.LiveData
import com.test.nsdl.mvvm.model.FavModel
import com.test.nsdl.mvvm.model.StoreModel
import com.test.nsdl.mvvm.room.StoreDatabase
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.launch

class StoreRepository {

    companion object {

        var storeDatabase: StoreDatabase? = null

        var storeModel: LiveData<List<StoreModel>>? = null
        var favModel: LiveData<FavModel>? = null

        fun initializeDB(context: Context): StoreDatabase {
            return StoreDatabase.getDataseClient(context)
        }

        fun insertData(context: Context, value: String, type: String) {

            storeDatabase =
                initializeDB(
                    context
                )

            CoroutineScope(IO).launch {
                val loginDetails =
                    StoreModel(value, type)
                storeDatabase!!.storeDao().InsertData(loginDetails)
            }

        }

        fun insertFavData(context: Context, pantId: String, shirtId: String) {

            storeDatabase =
                initializeDB(
                    context
                )

            CoroutineScope(IO).launch {
                val loginDetails =
                    FavModel(pantId, shirtId)
                storeDatabase!!.storeDao().InsertFavData(loginDetails)
            }

        }

        fun delFavData(context: Context, pantId: String, shirtId: String) {

            storeDatabase =
                initializeDB(
                    context
                )

            CoroutineScope(IO).launch {

                storeDatabase!!.storeDao().DeleteFav(pantId, shirtId)
            }

        }


        fun getFavData(context: Context, pantId: String, shirtId: String): LiveData<FavModel>? {


            storeDatabase =
                initializeDB(
                    context
                )

            favModel = storeDatabase!!.storeDao().getFavDat(pantId, shirtId)

            return favModel

        }


        fun getData(context: Context, type: String): LiveData<List<StoreModel>>? {

            storeDatabase =
                initializeDB(
                    context
                )

            storeModel = storeDatabase!!.storeDao().getData(type)

            return storeModel
        }

    }
}