package com.test.nsdl.mvvm.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.io.Serializable

@Entity(tableName = "Fav")
data class FavModel(

    @ColumnInfo(name = "paintId")
    var paintId: String,

    @ColumnInfo(name = "shirtId")
    var shirtId: String

) : Serializable {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    var Id: Int? = null

}