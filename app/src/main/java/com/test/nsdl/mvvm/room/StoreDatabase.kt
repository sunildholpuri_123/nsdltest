package com.test.nsdl.mvvm.room

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.test.nsdl.mvvm.model.FavModel
import com.test.nsdl.mvvm.model.StoreModel

@Database(
    entities = arrayOf(
        StoreModel::class,
        FavModel::class
    ), version = 1, exportSchema = false
)
abstract class StoreDatabase : RoomDatabase() {

    abstract fun storeDao(): DAOAccess

    companion object {

        @Volatile
        private var INSTANCE: StoreDatabase? = null

        fun getDataseClient(context: Context): StoreDatabase {

            if (INSTANCE != null) return INSTANCE!!

            synchronized(this) {

                INSTANCE = Room
                    .databaseBuilder(context, StoreDatabase::class.java, "Store")
                    .fallbackToDestructiveMigration()
                    .build()

                return INSTANCE!!

            }
        }

    }

}