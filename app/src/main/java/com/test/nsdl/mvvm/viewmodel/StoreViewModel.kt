package com.test.nsdl.mvvm.viewmodel

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.test.nsdl.mvvm.model.FavModel
import com.test.nsdl.mvvm.model.StoreModel
import com.test.nsdl.mvvm.repository.StoreRepository

class StoreViewModel : ViewModel() {

    var storeliveData: LiveData<List<StoreModel>>? = null
    var favliveData: LiveData<FavModel>? = null
    fun insertData(context: Context, value: String, type: String) {
        StoreRepository.insertData(context, value, type)
    }

    fun insertFavData(context: Context, value: String, type: String) {
        StoreRepository.insertFavData(context, value, type)
    }

    fun delFavItem(context: Context, value: String, type: String) {
        StoreRepository.delFavData(context, value, type)
    }



    fun getFavData(context: Context, value: String, type: String): LiveData<FavModel>? {
        favliveData = StoreRepository.getFavData(context, value, type)
        return favliveData
    }

    fun getData(context: Context, type: String): LiveData<List<StoreModel>>? {
        storeliveData = StoreRepository.getData(context, type)
        return storeliveData
    }

}