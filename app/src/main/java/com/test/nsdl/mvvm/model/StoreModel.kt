package com.test.nsdl.mvvm.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.io.Serializable

@Entity(tableName = "Store")
data class StoreModel(

    @ColumnInfo(name = "Value")
    var Value: String,

    @ColumnInfo(name = "Type")
    var Type: String

) : Serializable {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    var Id: Int? = null

}