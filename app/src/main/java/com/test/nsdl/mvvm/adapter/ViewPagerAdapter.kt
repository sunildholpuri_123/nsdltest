package com.test.nsdl.mvvm.adapter

import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.test.nsdl.mvvm.model.StoreModel
import java.util.*
import kotlin.collections.ArrayList

class ViewPagerAdapter(fragmentManager: FragmentManager) : FragmentPagerAdapter(fragmentManager) {

    val fragments: ArrayList<Fragment> = ArrayList()
    private var list = ArrayList<StoreModel>()

    fun addFragment(fragment: Fragment) {
        fragments.add(fragment)
    }

    fun setList(nlist: List<StoreModel>) {
        list.clear()
        list.addAll(nlist)
        notifyDataSetChanged()
    }

    fun getList(): ArrayList<StoreModel> {
        return list
    }

    override fun getItem(p0: Int): Fragment = fragments.get(p0)

    override fun getCount(): Int = fragments.size

    fun clearData() {
        fragments.clear()
    }


    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        super.destroyItem(container, position, `object`)
    }

    fun refresh() {
        val seed = System.currentTimeMillis()

        if (!fragments.isNullOrEmpty()) {
           /* Collections.shuffle(fragments, Random())
            list.shuffled(Random(seed))*/
            val rand = Random()
            val randomNum = rand.nextInt(list.size - 1 - 0 + 1) + 0
            list[randomNum]
            notifyDataSetChanged()
        }
    }
}